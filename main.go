package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/pkg/errors"
	clientset "k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	pvController "sigs.k8s.io/sig-storage-lib-external-provisioner/controller"
)

const (
	defaultNamespace       = "local-path"
	defaultConfigPath      = "/etc/local-path/config.json"
	defaultProvisionerName = "local-path.shasderias.com"
	defaultHelperImage     = "busybox"
)

type daemonContext struct {
	configPath      string
	provisionerName string
	namespace       string
	helperImage     string
}

func printUsage() {
	fmt.Fprintln(os.Stderr, "local-path [cmd]")
	fmt.Fprintln(os.Stderr, "  start - start the provisioner")
}

func main() {
	if len(os.Args) == 0 {
		printUsage()
		os.Exit(1)
	}

	cmdName := os.Args[1]

	type cmdFunc func([]string) error

	var cmd cmdFunc

	switch cmdName {
	case "start":
		cmd = StartCmd
	default:
		fmt.Fprintf(os.Stderr, "unrecognized command: %s", cmdName)
		os.Exit(1)
	}

	if err := cmd(os.Args[2:]); err != nil {
		fmt.Fprintf(os.Stderr, "error executing cmd '%s': %v", cmdName, err)
		os.Exit(1)
	}
}

func StartCmd(args []string) error {
	fs := flag.NewFlagSet("start", flag.ContinueOnError)

	var (
		configPath      = fs.String("config", defaultConfigPath, "Path to configuration file.")
		provisionerName = fs.String("name", defaultProvisionerName, "Provisioner name")
		namespace       = fs.String("namespace", defaultNamespace, "Namespace provisioner is running in.")
		helperImage     = fs.String("helper-image", defaultHelperImage, "Helper image used for creating/deleting directories on the host.")
	)
	if err := fs.Parse(args); err != nil {
		return err
	}

	return startDaemon(daemonContext{
		configPath:      *configPath,
		provisionerName: *provisionerName,
		namespace:       *namespace,
		helperImage:     *helperImage,
	})
}

func startDaemon(ctx daemonContext) error {
	config, err := rest.InClusterConfig()
	if err != nil {
		return errors.Wrap(err, "error getting client config")
	}

	kubeClient, err := clientset.NewForConfig(config)
	if err != nil {
		return errors.Wrap(err, "error getting k8s client")
	}

	serverVersion, err := kubeClient.Discovery().ServerVersion()
	if err != nil {
		return errors.Wrap(err, "error getting Kubernetes server version")
	}

	stopCh := make(chan struct{})
	registerShutdownChannel(stopCh)

	provisioner, err := NewProvisioner(stopCh, kubeClient, ctx.configPath, ctx.namespace, ctx.helperImage)
	if err != nil {
		return err
	}
	pc := pvController.NewProvisionController(
		kubeClient,
		ctx.provisionerName,
		provisioner,
		serverVersion.GitVersion,
	)
	log.Println("local-path provisioner started")
	pc.Run(stopCh)
	log.Println("local-path provisioner stopped")
	return nil
}

func registerShutdownChannel(done chan struct{}) {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		sig := <-sigs
		log.Printf("received signal: %v", sig)
		close(done)
	}()
}
