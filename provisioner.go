package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"reflect"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/pkg/errors"
	"k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	clientset "k8s.io/client-go/kubernetes"
	pvController "sigs.k8s.io/sig-storage-lib-external-provisioner/controller"
)

const (
	HelperPodMountPath = "/data"
	HelperPodTimeout   = 120
)

const (
	HostnameKey      = "kubernetes.io/hostname"
	UIDAnnotationKey = "local-path.shasderias.com/uid"
	GIDAnnotationKey = "local-path.shasderias.com/gid"
)

var (
	ConfigWatchInterval = 30 * time.Second
)

type LocalPathProvisioner struct {
	stopCh      chan struct{}
	kubeClient  *clientset.Clientset
	namespace   string
	helperImage string

	config      *Config
	configPath  string
	configMutex *sync.RWMutex
}

type Config struct {
	DefaultPath string            `json:"defaultPath"`
	NodePath    map[string]string `json:"nodePath"`
}

func NewProvisioner(stopCh chan struct{}, kubeClient *clientset.Clientset, configPath, namespace, helperImage string) (*LocalPathProvisioner, error) {
	p := &LocalPathProvisioner{
		stopCh: stopCh,

		kubeClient:  kubeClient,
		namespace:   namespace,
		helperImage: helperImage,

		// config will be updated shortly by p.refreshConfig()
		config:      nil,
		configPath:  configPath,
		configMutex: &sync.RWMutex{},
	}
	if err := p.refreshConfig(); err != nil {
		return nil, err
	}
	go p.watchConfig()
	return p, nil
}

func (p *LocalPathProvisioner) getNodePath(node string) (string, error) {
	p.configMutex.RLock()
	defer p.configMutex.RUnlock()

	if p.config == nil {
		return "", errors.New("no config available")
	}

	if _, ok := p.config.NodePath[node]; !ok {
		log.Printf("getNodePath(): no path configured for node '%s', using default path '%s'", node, p.config.DefaultPath)
		return p.config.DefaultPath, nil
	}

	return p.config.NodePath[node], nil
}

func (p *LocalPathProvisioner) Provision(opts pvController.ProvisionOptions) (pv *v1.PersistentVolume, err error) {
	defer func() {
		if err != nil {
			log.Printf("Provision(): error provisioning volume: %v", err)
		}
	}()
	pvc := opts.PVC

	if pvc.Spec.Selector != nil {
		return nil, errors.Errorf("Spec.Selector is not supported")
	}
	for _, accessMode := range pvc.Spec.AccessModes {
		if accessMode != v1.ReadWriteOnce {
			return nil, errors.Errorf("only ReadWriteOnce access mode is supported, got %s", accessMode)
		}
	}
	if opts.SelectedNode == nil {
		return nil, errors.New("expected SelectedNode to be passed to Provision()")
	}

	nodeHostname := opts.SelectedNode.Labels[HostnameKey]
	if nodeHostname == "" {
		return nil, errors.New("expected SelectedNode to be labeled with hostname")
	}

	uid, gid, err := parseUIDAndGID(pvc.Annotations)
	if err != nil {
		return nil, errors.Wrapf(err, "error parsing annotations for UID and GID: %v", pvc.Annotations)
	}

	nodePath, err := p.getNodePath(nodeHostname)
	if err != nil {
		return nil, err
	}

	if nodePath == "" {
		return nil, errors.Errorf("empty path configured for node '%s', not provisioning as forbidden by config")
	}

	pvDir := opts.PVName
	path := filepath.Join(HelperPodMountPath, pvDir)

	cmds := []string{}
	cmds = append(cmds, fmt.Sprintf("mkdir -m 0755 -p %s", path))
	if uid != -1 {
		cmds = append(cmds, fmt.Sprintf("chown %d %s", uid, path))
	}
	if gid != -1 {
		cmds = append(cmds, fmt.Sprintf("chgrp %d %s", gid, path))
	}

	log.Printf("Provision(): creating volume '%v' at '%v:%v' with commands '%s'",
		opts.PVName, nodeHostname, filepath.Join(nodePath, pvDir), cmds)
	if err := p.createHelperPod(nodeHostname, cmds, nodePath); err != nil {
		log.Printf("Provision(): helper pod error: %v", err)
		return nil, err
	}
	log.Printf("Provision(): volume '%v' created", opts.PVName)

	nodePVPath := filepath.Join(nodePath, pvDir)
	fs := v1.PersistentVolumeFilesystem
	return &v1.PersistentVolume{
		ObjectMeta: metav1.ObjectMeta{
			Name: pvDir,
		},
		Spec: v1.PersistentVolumeSpec{
			PersistentVolumeReclaimPolicy: *opts.StorageClass.ReclaimPolicy,
			AccessModes:                   pvc.Spec.AccessModes,
			VolumeMode:                    &fs,
			Capacity: v1.ResourceList{
				v1.ResourceName(v1.ResourceStorage): pvc.Spec.Resources.Requests[v1.ResourceName(v1.ResourceStorage)],
			},
			PersistentVolumeSource: v1.PersistentVolumeSource{
				Local: &v1.LocalVolumeSource{Path: nodePVPath},
			},
			NodeAffinity: &v1.VolumeNodeAffinity{
				Required: &v1.NodeSelector{
					NodeSelectorTerms: []v1.NodeSelectorTerm{
						{
							MatchExpressions: []v1.NodeSelectorRequirement{
								{
									Key:      HostnameKey,
									Operator: v1.NodeSelectorOpIn,
									Values:   []string{nodeHostname},
								},
							},
						},
					},
				},
			},
		},
	}, nil
}

func (p *LocalPathProvisioner) Delete(pv *v1.PersistentVolume) (err error) {
	defer func() {
		if err != nil {
			log.Printf("Delete(): error deleting volume: %v", err)
		}
	}()

	switch pv.Spec.PersistentVolumeReclaimPolicy {
	case v1.PersistentVolumeReclaimDelete, v1.PersistentVolumeReclaimRecycle:
		nodeHostname, err := getPVNodeHostname(pv)
		if err != nil {
			return err
		}

		path, nodePath, pvDir, err := getPVNodePathAndPVDir(pv)
		if err != nil {
			return err
		}

		cmds := []string{fmt.Sprintf("rm -rf '%s'", filepath.Join(HelperPodMountPath, pvDir))}

		log.Printf("Delete(): deleting volume '%v' at '%v:%v' with commands '%s'",
			pv.Name, nodeHostname, path, cmds)

		if err := p.createHelperPod(nodeHostname, cmds, nodePath); err != nil {
			log.Printf("error cleaning up volume '%v': %v", pv.Name, err)
			return err
		}
		log.Printf("Delete(): volume '%v' deleted", pv.Name)
	case v1.PersistentVolumeReclaimRetain:
		log.Printf("Delete(): volume '%v' retained", pv.Name)
	default:
		log.Printf("Delete(): unexpected PersistentVolumeReclaimPolicy '%v', doing nothing",
			pv.Spec.PersistentVolumeReclaimPolicy)
	}

	return nil
}

func getPVNodePathAndPVDir(pv *v1.PersistentVolume) (path string, nodePath string, pvDir string, err error) {
	defer func() {
		if r := recover(); r != nil {
			err = errors.Wrapf(err, "malformed Spec: %v", pv.Spec)
		}
	}()

	path = pv.Spec.PersistentVolumeSource.Local.Path
	if path == "" {
		err = errors.New("expected Spec.PersistentVolumeSource.Local.Path to not be empty")
		return
	}

	path, err = filepath.Abs(path)
	if err != nil {
		return
	}

	nodePath, pvDir = filepath.Split(path)
	if nodePath == "/" || nodePath == "" {
		err = errors.Errorf("nodePath cannot be '/' or '', path: %s", path)
		return
	}

	if pvDir == "." {
		err = errors.Errorf("pvDir cannot be '.', path: %s", path)
		return
	}

	return
}

// getPVNodeHostname returns the hostname of the node pv is provisioned on, used by Delete()
func getPVNodeHostname(pv *v1.PersistentVolume) (nodeHostname string, err error) {
	defer func() {
		if r := recover(); r != nil {
			err = errors.Errorf("malformed Spec: %v", r)
			return
		}
	}()

	var (
		nodeSelectorTerms = pv.Spec.NodeAffinity.Required.NodeSelectorTerms
		matchExpressions  = pv.Spec.NodeAffinity.Required.NodeSelectorTerms[0].MatchExpressions
		expression        = pv.Spec.NodeAffinity.Required.NodeSelectorTerms[0].MatchExpressions[0]
	)

	if len(nodeSelectorTerms) != 1 {
		return "", errors.New("expected Spec.NodeAffinity.Required.NodeSelectorTerms to contain exactly 1 term")
	}

	if len(matchExpressions) != 1 {
		return "", errors.New("expected Spec.NodeAffinity.Required.NodeSelectorTerms[0].MatchExpressions to contain exactly 1 expression")
	}

	if expression.Key != HostnameKey {
		return "", errors.Errorf("expected MatchExpression key to be %s", HostnameKey)
	}

	if expression.Operator != v1.NodeSelectorOpIn {
		return "", errors.Errorf("expected MatchExpression operator to be %s", v1.NodeSelectorOpIn)
	}

	if len(expression.Values) != 1 {
		return "", errors.New("expected MatchExpressions to have exactly 1 value")
	}

	nodeHostname = expression.Values[0]

	if nodeHostname == "" {
		return "", errors.Errorf("unable to find node in spec: %v", pv.Spec)
	}
	return nodeHostname, nil
}

func (p *LocalPathProvisioner) createHelperPod(node string, cmds []string, nodePath string) error {
	podName := fmt.Sprintf("local-path-helper-%s", node)

	cmdStr := strings.Join(cmds, ";")
	cmd := []string{"/bin/sh", "-c", cmdStr}

	hostPathType := v1.HostPathDirectoryOrCreate
	helperPod := &v1.Pod{
		ObjectMeta: metav1.ObjectMeta{
			Name: podName,
		},
		Spec: v1.PodSpec{
			RestartPolicy: v1.RestartPolicyNever,
			NodeName:      node,
			Tolerations:   []v1.Toleration{{Operator: v1.TolerationOpExists}},
			Containers: []v1.Container{
				{
					Name:    podName,
					Image:   p.helperImage,
					Command: cmd,
					VolumeMounts: []v1.VolumeMount{
						{
							Name:      "data",
							ReadOnly:  false,
							MountPath: HelperPodMountPath,
						},
					},
					ImagePullPolicy: v1.PullIfNotPresent,
				},
			},
			Volumes: []v1.Volume{
				{
					Name: "data",
					VolumeSource: v1.VolumeSource{
						HostPath: &v1.HostPathVolumeSource{
							Path: nodePath,
							Type: &hostPathType,
						},
					},
				},
			},
		},
	}

	pod, err := p.kubeClient.CoreV1().Pods(p.namespace).Create(helperPod)
	if err != nil {
		return err
	}

	defer func() {
		delErr := p.kubeClient.CoreV1().Pods(p.namespace).Delete(pod.Name, &metav1.DeleteOptions{})
		if delErr != nil {
			if err == nil {
				err = errors.Errorf("error deleting helper pod: %v", delErr)
			} else {
				err = errors.Wrapf(err, "error deleting helper pod: %v", delErr)
			}
		}
	}()

	completed := false
	for i := 0; i < HelperPodTimeout; i++ {
		if pod, err := p.kubeClient.CoreV1().Pods(p.namespace).Get(pod.Name, metav1.GetOptions{}); err != nil {
			return err
		} else if pod.Status.Phase == v1.PodSucceeded {
			completed = true
			break
		}
		time.Sleep(1 * time.Second)
	}
	if !completed {
		return fmt.Errorf("helper pod failed to succeed after %v seconds", HelperPodTimeout)
	}

	return nil
}

func (p *LocalPathProvisioner) refreshConfig() error {
	p.configMutex.Lock()
	defer p.configMutex.Unlock()

	newConfig, err := loadConfig(p.configPath)
	if err != nil {
		return err
	}

	if reflect.DeepEqual(p.config, newConfig) {
		return nil
	}

	p.config = newConfig
	log.Println("refreshConfig(): new config loaded")

	return nil
}

func (p *LocalPathProvisioner) watchConfig() {
	for {
		select {
		case <-time.Tick(ConfigWatchInterval):
			if err := p.refreshConfig(); err != nil {
				log.Printf("watchConfig(): error loading config: %v\n", err)
			}
		case <-p.stopCh:
			log.Println("watchConfig(): config watcher stopped")
			return
		}
	}
}

func loadConfig(configPath string) (config *Config, err error) {
	defer func() {
		err = errors.Wrapf(err, "error loading config from '%s'", configPath)
	}()

	f, err := os.Open(configPath)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	if err := json.NewDecoder(f).Decode(&config); err != nil {
		return nil, err
	}

	if err := validateConfig(config); err != nil {
		return nil, err
	}

	return config, nil
}

func validateConfig(config *Config) (err error) {
	defer func() {
		err = errors.Wrapf(err, "error validating config: %v", config)
	}()

	canonicalDefaultPath, err := canonicalizeAndValidatePath(config.DefaultPath)
	if err != nil {
		return errors.Errorf("error validating default path: %v", err)
	}
	config.DefaultPath = canonicalDefaultPath

	for node, path := range config.NodePath {
		path, err = canonicalizeAndValidatePath(path)
		if err != nil {
			return errors.Errorf("error validating path for node '%s': %v", node, err)
		}
		config.NodePath[node] = path
	}

	return nil
}

func canonicalizeAndValidatePath(path string) (string, error) {
	if path == "" {
		return "", nil
	}
	if path[0] != '/' {
		return "", fmt.Errorf("path '%s' is not absolute", path)
	}
	canonicalPath, err := filepath.Abs(path)
	if err != nil {
		return "", err
	}
	if canonicalPath == "/" {
		return "", fmt.Errorf("path '%s' is filesystem root", path)
	}
	if canonicalPath == "." { // TODO: is this necessary? will this ever evaluate to true?
		return "", fmt.Errorf("path '%s' resolves to '.'", path)
	}
	return canonicalPath, nil
}

func parseUIDAndGID(annotations map[string]string) (uid, gid int, err error) {
	uid = -1
	gid = -1

	uidStr := annotations[UIDAnnotationKey]
	if uidStr != "" {
		uid, err = strconv.Atoi(uidStr)
		if err != nil {
			return
		}
	}

	gidStr := annotations[GIDAnnotationKey]
	if gidStr != "" {
		gid, err = strconv.Atoi(gidStr)
		if err != nil {
			return
		}
	}

	return uid, gid, nil
}
