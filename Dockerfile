FROM golang:1.12.9-alpine3.10 as builder

WORKDIR /local-path

COPY . .

RUN go build -mod=vendor gitlab.com/shasderias/local-path

FROM alpine:3.10

COPY --from=builder /local-path/local-path /usr/local/bin/

ENTRYPOINT ["local-path"]