package localpath

objects: [ x for v in objectSets for x in v ]

objectSets: [
	namespace,
	configMap,
	deployment,
	serviceAccount,
	clusterRoleBinding,
	clusterRole,
	storageClass,
	pvc,
	pod,
]
