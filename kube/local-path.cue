package localpath

_base: {
	_name: string
	metadata name: _name
}

_namespaced_base: _base & {
	metadata namespace: _namespace
}

namespace <Name>: _base & {
	_name: Name

	apiVersion: "v1"
	kind:       "Namespace"
}

serviceAccount <Name>: _namespaced_base & {
	_name: Name

	apiVersion: "v1"
	kind:       "ServiceAccount"
}

_rbacAPIVersion: "rbac.authorization.k8s.io/v1"
clusterRole <Name>: _namespaced_base & {
	_name: Name

	apiVersion: _rbacAPIVersion
	kind:       "ClusterRole"
}

clusterRoleBinding <Name>: _namespaced_base & {
	_name: Name

	apiVersion: _rbacAPIVersion
	kind:       "ClusterRoleBinding"
}

deployment <Name>: _namespaced_base & {
	_name: Name

	apiVersion: "apps/v1"
	kind:       "Deployment"

	_baseLabels: {
		"app.kubernetes.io/name": _app
	}

	metadata labels: _baseLabels

	spec selector matchLabels: _baseLabels
	spec template metadata labels: _baseLabels

	spec: {
		template spec containers: [{
			name: _name
		}]
	}
}

configMap <Name>: _namespaced_base & {
	_name: Name

	apiVersion: "v1"
	kind:       "ConfigMap"
}

storageClass <Name>: _base & {
	_name: Name

	apiVersion:        "storage.k8s.io/v1"
	kind:              "StorageClass"
	volumeBindingMode: "WaitForFirstConsumer"
	provisioner:       _provisioner
	reclaimPolicy:     "Delete" | *"Retain"
}

pod <Name>: _base & {
	_name: Name

	apiVersion: "v1"
	kind:       "Pod"
}

pvc <Name>: _base & {
	_name: Name

	apiVersion: "v1"
	kind:       "PersistentVolumeClaim"
}

localPathPVC <Name>: {
	namespace:      string | *null
	uid:            int | *null
	gid:            int | *null
	storageClass:   string
	storageRequest: string
}

pvc: {
	"\(n)": {
		_name: n
		metadata namespace: v.namespace if v.namespace != null
		metadata annotations?: {
			"\(provisioner)/uid": strconv.Atoi(uid) if v.uid != null
			"\(provisioner)/gid": strconv.Atoi(gid) if v.gid != null
		}
		spec accessModes: ["ReadWriteOnce"]
		spec storageClassName: v.storageClass
		spec resources requests storage: v.storageRequest
	} for n, v in localPathPVC
}

testPod <Name>: {
	namespace: string
	nodeName:  string
	image:     string
	pvcName:   string
}

pod: {
	"\(n)": {
		_name: n
		metadata namespace: v.namespace if v.namespace != null
		spec: {
			affinity nodeAffinity requiredDuringSchedulingIgnoredDuringExecution nodeSelectorTerms: [{
				matchFields: [{
					key:      "metadata.name"
					operator: "In"
					values: ["kube-plex"]
				}]
			}]
			containers: [{
				name:  n
				image: v.image
				volumeMounts: [{
					name:      "volume"
					mountPath: "/data"
				}]
			}]
			volumes: [{
				name: "volume"
				persistentVolumeClaim claimName: v.pvcName
			}]
		}
	} for n, v in testPod
}
