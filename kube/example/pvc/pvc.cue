package localpath

localPathPVC "localpath-test": {
	uid:            3333
	gid:            3333
	storageClass:   "local-path"
	storageRequest: "2Gi"
}
