package localpath

testPod "local-path-test": {
	nodeName: "kube-plex"
	image:    "nginx:stable-alpine"
	pvcName:  "local-path-test"
}
