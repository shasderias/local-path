package localpath

import "encoding/json"

_app:         "local-path"
_namespace:   _app
_image:       "registry.gitlab.com/shasderias/local-path:latest"
_provisioner: "local-path.shasderias.com"

// TODO: pin image tag to a version once stable

namespace "\(_app)": {}

serviceAccount "\(_app)": {}

clusterRole "\(_app)": {
	rules: [{
		apiGroups: [""]
		resources: ["nodes", "persistentvolumeclaims"]
		verbs: ["get", "list", "watch"]
	}, {
		apiGroups: [""]
		resources: ["endpoints", "persistentvolumes", "pods"]
		verbs: ["*"]
	}, {
		apiGroups: [""]
		resources: ["events"]
		verbs: ["create", "patch"]
	}, {
		apiGroups: ["storage.k8s.io"]
		resources: ["storageclasses"]
		verbs: ["get", "list", "watch"]
	}]
}

clusterRoleBinding "\(_app)": {
	_role: clusterRole["\(_app)"]
	roleRef: {
		apiGroup: "rbac.authorization.k8s.io"
		kind:     _role.kind
		name:     _role.metadata.name
	}

	_serviceAccount = serviceAccount["\(_app)"]
	subjects: [{
		kind:      _serviceAccount.kind
		name:      _serviceAccount.metadata.name
		namespace: _serviceAccount.metadata.namespace
	}]
}

deployment "\(_app)": {
	spec: {
		replicas: 1
		template: {
			spec: {
				serviceAccountName: serviceAccount["\(_app)"].metadata.name
				containers: [{
					image:           _image
					imagePullPolicy: "IfNotPresent"
					args: ["start"]
					volumeMounts: [{
						name:      "config"
						mountPath: "/etc/\(_app)/"
					}]
					env: [{
						name: "POD_NAMESPACE"
						valueFrom fieldRef fieldPath: "metadata.namespace"
					}]
				}]
				volumes: [{
					name: "config"
					configMap name: "\(_app)"
				}]
			}
		}
	}
}

storageClass "\(_app)": {
	reclaimPolicy: "Delete"
}

configMap "\(_app)": {
	data: {
		"config.json": json.Marshal(_config_json)
		_config_json = {
			defaultPath: "/opt/local-path"
			nodePath: {
				"example-node-name": "/opt/example-node-path"
			}
		}
	}
}
